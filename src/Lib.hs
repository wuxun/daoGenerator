{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( someFunc
    ) where

import Data.Configurator
import Data.Configurator.Types
import Data.List (intercalate)
import Data.List.Split
import Database.HDBC
import Database.HDBC.MySQL
import System.Directory
import qualified Data.Char as Char

data DataConfig = DataConfig {
  tableName :: String,
  entityName :: String,
  entityPackageName :: String,
  daoName :: String,
  daoPackageName :: String,
  daoCatalog :: String
  } deriving (Show)

someFunc :: IO ()
someFunc = do
  config <- loadConfig "config.conf"
  display config

  host <- lookupDefault "127.0.0.1" config "db_host"
  port <- lookupDefault 3306 config "db_port"
  username <- require config "db_username"
  password <- require config "db_password"
  dbname <- require config "db_name"

  conn <- connectMySQL defaultMySQLConnectInfo {
    mysqlHost = host,
    mysqlPort = port,
    mysqlUser = username,
    mysqlPassword = password,
    mysqlDatabase = dbname};

  tableName <- require config "table_name"

  fields <- describeTable conn tableName

  getCurrentDirectory >>= putStrLn

  let dbFields = map fst fields
  let javaFields = map toFieldName dbFields
  let javaTypes = (map (toJavaType . colType . snd) fields)

  clsName <- require config "entity_name"
  clsPackageName <- require config "entity_package_name"
  daoName <- lookupDefault (clsName ++ "DAO") config "dao_name"
  daoPackageName <- lookupDefault (clsPackageName ++ ".dao") config "dao_package_name"
  daoCatalog <- require config "dao_catalog"

  let config = DataConfig {
        tableName = tableName,
        entityName = clsName,
        entityPackageName = clsPackageName,
        daoName = daoName,
        daoPackageName = daoPackageName,
        daoCatalog = daoCatalog};

  putStrLn $ show (map fst fields)
  putStrLn $ show javaFields
  putStrLn $ show javaTypes

  let outputDir = "target"
  createDirectoryIfMissing True outputDir

  writeFile (outputDir ++ "/" ++ clsName ++ ".java") (genModeFile config javaFields javaTypes)
  writeFile (outputDir ++ "/" ++ daoName ++ ".java") (genDAO config dbFields javaFields)

loadConfig :: FilePath -> IO Config
loadConfig file = load [Required file]

toJavaType :: SqlTypeId -> String
toJavaType sqlType =
  case sqlType of
    SqlCharT -> "String"
    SqlVarCharT -> "String"

    SqlTinyIntT -> "Integer"
    SqlSmallIntT -> "Integer"
    SqlIntegerT -> "Integer"

    SqlFloatT -> "Double"
    SqlDoubleT -> "Double"

    SqlTimestampT -> "Timestamp"

    otherwise -> "String"


toFieldName :: String -> String
toFieldName colum = uncaptitalized $ concatMap captitalized $ splitOn "_" colum

captitalized :: String -> String
captitalized [] = []
captitalized (head:tail) = Char.toUpper head : tail

uncaptitalized :: String -> String
uncaptitalized [] = []
uncaptitalized (head:tail) = Char.toLower head : tail

genModeFile :: DataConfig -> [String] -> [String] -> String
genModeFile config fields types =
  genPackage (entityPackageName config) ++ "\n\n" ++
  genClassStart (entityName config) ++ "\n\n" ++
  genFields fields types ++ "\n\n" ++
  genGettersSetters fields types ++ "\n" ++
  genClassEnd

genPackage :: String -> String
genPackage p = "package " ++ p ++ ";"

genClassStart :: String -> String
genClassStart cls = "public class " ++ cls ++ " {"

genClassEnd :: String
genClassEnd = "}"

genFields :: [String] -> [String] -> String
genFields fields types = intercalate "\n" allFields
  where
    allFields = zipWith genField fields types
    genField fieldName typeName = indent ++ "private " ++ typeName ++ " " ++ fieldName ++ ";"

genGettersSetters :: [String] -> [String] -> String
genGettersSetters fields types = intercalate "\n\n" $ zipWith genGetterSetter fields types
  where
    genGetterSetter fieldName typeName = genGetter fieldName typeName ++ "\n\n" ++ genSetter fieldName typeName
    genGetter fieldName typeName =
      indent ++ "public " ++ typeName ++ " get" ++ (captitalized fieldName) ++ "() {" ++ "\n" ++
      indent ++ indent ++ "return " ++ fieldName ++ ";\n" ++
      indent ++ "}"
    genSetter fieldName typeName =
      indent ++ "public void set" ++ (captitalized fieldName) ++ "("++typeName ++ " " ++fieldName ++") {" ++ "\n" ++
      indent ++ indent ++ "this." ++ fieldName ++ " = " ++ fieldName ++ ";\n" ++
      indent ++ "}"

indent :: String
indent = "    "

genDAO :: DataConfig -> [String] -> [String] -> String
genDAO config dbFields clsFields =
  genPackage (daoPackageName config) ++ "\n\n" ++
  genDaoImport ++ "\n" ++
  genDAOInterfaceStart (daoName config) (daoCatalog config)++ "\n" ++
  genDAOFields (tableName config) dbFields clsFields ++ "\n\n" ++
  genDAOMethod (entityName config) ++ "\n" ++
  genDAOInterfaceEnd

genDaoImport :: String
genDaoImport =
  "import net.paoding.rose.jade.annotation.DAO;" ++ "\n"
  ++ "import net.paoding.rose.jade.annotation.SQL;" ++ "\n"

genDAOInterfaceStart :: String -> String -> String
genDAOInterfaceStart daoName catalog =
  "@DAO(catalog = \"" ++ catalog ++ "\")" ++ "\n" ++
  "public interface " ++ daoName ++ " {"

genDAOFields :: String -> [String] -> [String] -> String
genDAOFields tableName dbFields clsFields =
  indent ++ fieldPrefix ++ "TABLE_NAME = \"" ++ tableName ++ "\";\n" ++
  indent ++ fieldPrefix ++ "COLUMNS = \"" ++ (intercalate "," dbFields) ++ "\";\n" ++
  indent ++ fieldPrefix ++ "FIELDS = \"" ++ (intercalate "," (map (\f -> ":1." ++ f) clsFields)) ++ "\";"

  where
    fieldPrefix = "public static final String "

genDAOMethod :: String -> String
genDAOMethod modelName =
  indent ++ "@SQL(\"insert into \" + TABLE_NAME + \"(\" + COLUMNS + \") values (\" + FIELDS + \")\")" ++ "\n" ++
  indent ++ "public int insert(" ++ modelName ++ " " ++ (uncaptitalized modelName) ++ ");"

genDAOInterfaceEnd :: String
genDAOInterfaceEnd = "}"
