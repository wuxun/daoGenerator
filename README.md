# daoGenerator

## 用法
### 安装stack
    https://docs.haskellstack.org/en/stable/install_and_upgrade/#linux
### clone 项目
    git clone git@gitlab.com:wuxun/daoGenerator.git
### 编译工程
    stack build
### 修改配置文件config.conf中的参数
```config
db_host = "127.0.0.1"
db_port = 3306
db_username = "root"
db_password = "123456"

# 数据库名称
db_name = "test"

# 表名称
table_name = "Orders"

# 表对应生成实体的名称
entity_name = "Order"
# 表对应实体的package
entity_package_name = "test.package"

# 表对应的DAO文件名称
dao_name = "OrderDAO"
# DAO文件的package name
dao_package_name = "test.package.dao"
```

### 运行项目，生成对应实体和DAO文件
    stack exec daoGenerator-exe
### 检查target文件夹下生成的文件

### 效果
* sql
```Sql
CREATE TABLE `Orders` (
  `F_request_no` int(11) NOT NULL COMMENT '注释',
  `F_mrchnt_no` varchar(20) DEFAULT NULL,
  `F_create_time` datetime DEFAULT NULL,
  `F_id` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`F_request_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

* model
```Java
package test.package;

class Order {

    private Integer fRequestNo;
    private String fMrchntNo;
    private Timestamp fCreateTime;
    private Integer fId;

    public Integer getFRequestNo() {
        return fRequestNo;
    }

    public void setFRequestNo(Integer fRequestNo) {
        this.fRequestNo = fRequestNo;
    }

    public String getFMrchntNo() {
        return fMrchntNo;
    }

    public void setFMrchntNo(String fMrchntNo) {
        this.fMrchntNo = fMrchntNo;
    }

    public Timestamp getFCreateTime() {
        return fCreateTime;
    }

    public void setFCreateTime(Timestamp fCreateTime) {
        this.fCreateTime = fCreateTime;
    }

    public Integer getFId() {
        return fId;
    }

    public void setFId(Integer fId) {
        this.fId = fId;
    }
}
```

* DAO
```Java
package test.package.dao;

@DAO
public interface Orders {
    public static final String TABLE_NAME = "Orders";
    public static final String COLUMNS = "F_request_no,F_mrchnt_no,F_create_time,F_id";
    public static final String FIELDS = ":1.fRequestNo,:1.fMrchntNo,:1.fCreateTime,:1.fId";

    @SQL("insert into " + TABLE_NAME + "(" + COLUMNS + ") values (" + FIELDS + ")")
    public int insert(Order order);
}
```



### TODO
1. 格式化文件
2. import支持
3. 类型/对应属性映射配置
4. 注释生成
5. 添加更多方法